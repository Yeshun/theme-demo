<?php

/*** 配置 ***/
return [

    /*** 主题标识的惟一名称 ***/
    'name' => 'theme-demo',

    /*** 菜单位置 ***/
    'menus' => [

        'main' => '主菜单',
        'sidemenu' => '侧边菜单'

    ],

    /*** 小部件位置 ***/
    'positions' => [

        'navbar' => '导航栏',
        'login' => '用户登录',
        'hero' => '英雄',
        'sidebar' => '侧边栏',
        'footer' => '页脚',
        'sidemenu' => '小部件侧边菜单'

    ],

   
    /*** 配置默认值 ***/
    'config' => [
        
        'mobile_logo' => '',
        'sidebar_logo' => ''
    ],
    
];
